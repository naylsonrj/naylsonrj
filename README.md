# I'm Naylson Costa

Brazilian front-end developer passionate to learn new technologies and create stuff.<br>

Currently IT undergrad student at Estácio de Sá and working as a junior dev with Angular, TypeScript, PHP, NodeJS, SQLServer, HTML, CSS, Boostrap, Gitlab and RESTful API’s development.
<br>

Also a police officer for the last 12 years working both in the field and on administrative roles.
<br><br>


<div align="center">
  <a href="https://github.com/naylsonrj">
     
</div>
<div style="display: inline_block" align="center"><br>
  <img align="center" alt="HTML" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="CSS" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
    <img align="center" alt="Bootstrap" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-plain-wordmark.svg">
  <img align="center" alt="Js" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">

  <img align="center" alt="typescript" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg">
   <img align="center" alt="Angularjs" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/angularjs/angularjs-original.svg">
  <img align="center" alt="React" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
   <img align="center" alt="Express" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/express/express-original.svg">
   <img align="center" alt="Nodejs" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg">
     <img align="center" alt="mysql" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original.svg">
  <img align="center" alt="Php" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-original.svg">
<!-- <img align="center" alt="Laravel" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/laravel/laravel-plain.svg"> -->  
  <img align="center" alt="Sass" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/sass/sass-original.svg">

</div><br>
  
##
<div align="center">
  
<a href="https://www.linkedin.com/in/naylsoncosta/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
<a href="https://github.com/naylsonrj" target="_blank"><img src="https://img.shields.io/badge/-github-%23000000?style=for-the-badge&logo=github&logoColor=white" target="_blank"></a>


<!-- <img align="center" alt="Naylson Costa" width="300" height="456" style="border-radius:50px;" src="https://i.picasion.com/pic92/a8af93cdbb94e67188dc5b950f3f637a.gif"> -->
</div>
<br>



</div>

